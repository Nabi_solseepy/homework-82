const express = require('express');
const Tracks = require('../models/Track');

const router = express.Router();
router.get('/',(req,res) =>{
    let query;
    if (req.query.album) {
        query = {album: req.query.album}
    }
    Tracks.find(query).populate('album')
        .then(tracks => res.send(tracks))
        .catch(() => res.sendStatus(500))

});





module.exports = router;