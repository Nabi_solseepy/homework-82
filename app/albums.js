const express = require('express');
const nanoid = require('nanoid');
const multer = require('multer');
const config =require('../config');

const path = require('path');

const  Album =require('../models/Album');

const  router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cd) => {
        cd(null , config.uploadPath);
    },
    filename: (req, file, cd) => {
        cd(null , nanoid() + path.extname(file.originalname));
    }
});
const upload = multer({storage});

router.get('/',(req,res) =>{
    let query;

    if (req.query.artist) {
        query = {artist: req.query.artist};
    }
    Album.find(query).sort({year: 1}).populate('artist')
        .then(album => res.send(album))
        .catch(() => res.sendStatus(500))

});
router.get('/:id',(req,res) =>{
    Album.findById(req.params.id).populate('artist')
        .then(album => res.send(album))
        .catch(() => res.sendStatus(500))

});


router.post('/',upload.single('image'), (req, res) => {
    const albumData = req.body;

    if (req.file){
        albumData.image = req.file.filename;
    }

    const album = new Album(albumData);


    album.save()
        .then(result => send(result))
        .catch(error => res.sendStatus(400).send(error))
});


module.exports = router;