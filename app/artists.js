const express = require('express');
const nanoid = require('nanoid');
const multer = require('multer');
const config =require('../config');
const Artist = require('../models/Artist');

const path = require('path');

const storage = multer.diskStorage({
    destination: (req, file, cd) => {
        cd(null , config.uploadPath);
    },
    filename: (req, file, cd) => {
        cd(null , nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();


router.get('/',(req,res) =>{
    Artist.find()
    .then(artists => res.send(artists))
    .catch(() => res.sendStatus(500))

});

router.post('/',upload.single('image'), (req, res) => {
    const artistData = req.body;

    if (req.file) {
        artistData.image = req.file.filename
    }

    const artist = new Artist(artistData);

    artist.save()
        .then(result => res.send(result))
        .catch(error => res.sendStatus(400).send(error))
});
module.exports = router;